const std = @import("std");
const os = std.os;
const net = @import("net.zig");
const builtin = @import("builtin");
const tbox = @import("tbox.zig");
const c = @import("c.zig");

pub fn main() void {
    const Client = struct {
        pub fn run(priv: c.tb_cpointer_t) callconv(.C) void {
            var client: *const net.StreamServer.Connection = @ptrCast(@alignCast(priv.?));
            runZ(client) catch |err| @panic(@errorName(err));
        }

        fn runZ(client: *const net.StreamServer.Connection) !void {
            var buffer: [13]u8 = undefined;
            defer client.stream.close();
            var len = try client.stream.read(&buffer);
            _ = try client.stream.write(buffer[0..len]);
        }
    };

    const Server = struct {
        pub fn run(priv: c.tb_cpointer_t) callconv(.C) void {
            _ = priv;
            runZ() catch |err| @panic(@errorName(err));
        }

        fn runZ() !void {
            const localhost = try std.net.Address.parseIp("127.0.0.1", 8080);
            var server = net.StreamServer.init(.{ .reuse_address = true });
            defer server.deinit();
            try server.listen(localhost);
            while (true) {
                var client = try server.accept();
                std.debug.print("client: {d}\n", .{client.stream.handle});
                if (c.tb_coroutine_start(null, &Client.run, &client, 0) <= 0) break;
            }
        }
    };

    _ = c.tb_init(null, c.tb_native_allocator());
    defer c.tb_exit();
    var scheduler = c.tb_co_scheduler_init();
    if (scheduler != null) {
        defer c.tb_co_scheduler_exit(scheduler);
        _ = c.tb_coroutine_start(scheduler, &Server.run, null, 0);
        c.tb_co_scheduler_loop(scheduler, c.tb_true);
    }
}
