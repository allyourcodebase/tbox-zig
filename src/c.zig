const builtin = @import("builtin");
pub usingnamespace @cImport({
    if (builtin.mode == .Debug) {
        @cDefine("__tb_debug__", "1");
    }
    @cDefine("__tb_small__", "1");
    @cInclude("tbox.h");
});
