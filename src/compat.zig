const std = @import("std");
const builtin = @import("builtin");

const in6_addr = extern struct {
    u: extern union {
        Byte: [16]u8,
        Word: [8]c_ushort,
    },
};

const in6addr_any = [1]u8{0} ** @sizeOf(std.c.in6_addr);

comptime {
    if (builtin.os.tag == .windows) {
        @export(in6addr_any, .{ .name = "in6addr_any" });
        @export(IN6_IS_ADDR_MC_LINKLOCAL, .{ .name = "IN6_IS_ADDR_MC_LINKLOCAL" });
        @export(IN6_IS_ADDR_LINKLOCAL, .{ .name = "IN6_IS_ADDR_LINKLOCAL" });
    }
}

inline fn IN6_IS_ADDR_MULTICAST(a: [*c]const in6_addr) bool {
    return a.*.u.Byte[0] == 0xff;
}

fn IN6_IS_ADDR_LINKLOCAL(a: [*c]const in6_addr) callconv(.C) c_int {
    return @intFromBool(a.*.u.Byte[0] == 0xf and (a.*.u.Byte[1] & 0xc0) == 0x80);
}

fn IN6_IS_ADDR_MC_LINKLOCAL(a: [*c]const in6_addr) callconv(.C) c_int {
    return @intFromBool(IN6_IS_ADDR_MULTICAST(a) and (a.*.u.Byte[1] & 0xf) == 2);
}
