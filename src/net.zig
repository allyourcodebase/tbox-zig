const std = @import("std");
const io = std.io;
const os = std.os;
const mem = std.mem;
const builtin = @import("builtin");
const c = @import("c.zig");
const tbox = @import("tbox.zig");
const assert = std.debug.assert;
const Address = std.net.Address;

pub const Stream = struct {
    // Underlying socket descriptor.
    // Note that on some platforms this may not be interchangeable with a
    // regular files descriptor.
    handle: os.socket_t,

    pub fn close(self: Stream) void {
        tbox.closeSocket(self.handle);
    }

    pub const ReadError = os.RecvFromError;
    pub const WriteError = os.SendToError;

    pub const Reader = io.Reader(Stream, ReadError, read);
    pub const Writer = io.Writer(Stream, WriteError, write);

    pub fn reader(self: Stream) Reader {
        return .{ .context = self };
    }

    pub fn writer(self: Stream) Writer {
        return .{ .context = self };
    }

    pub fn read(self: Stream, buffer: []u8) ReadError!usize {
        while (true) {
            var len = os.recv(self.handle, buffer, 0) catch |err| switch (err) {
                error.WouldBlock => {
                    var wait = c.tb_socket_wait(
                        tbox.sockFromFd(self.handle),
                        c.TB_SOCKET_EVENT_SEND,
                        -1,
                    );
                    if (wait <= 0) return error.Unexpected;
                    continue;
                },
                else => |e| return e,
            };
            return len;
        }
    }

    pub fn readv(s: Stream, iovecs: []const os.iovec) ReadError!usize {
        if (iovecs.len == 0) return @as(usize, 0);
        const first = iovecs[0];
        return s.read(s.handle, first.iov_base[0..first.iov_len]);
    }

    /// Returns the number of bytes read. If the number read is smaller than
    /// `buffer.len`, it means the stream reached the end. Reaching the end of
    /// a stream is not an error condition.
    pub fn readAll(s: Stream, buffer: []u8) ReadError!usize {
        return readAtLeast(s, buffer, buffer.len);
    }

    /// Returns the number of bytes read, calling the underlying read function
    /// the minimal number of times until the buffer has at least `len` bytes
    /// filled. If the number read is less than `len` it means the stream
    /// reached the end. Reaching the end of the stream is not an error
    /// condition.
    pub fn readAtLeast(s: Stream, buffer: []u8, len: usize) ReadError!usize {
        assert(len <= buffer.len);
        var index: usize = 0;
        while (index < len) {
            const amt = try s.read(buffer[index..]);
            if (amt == 0) break;
            index += amt;
        }
        return index;
    }

    /// TODO in evented I/O mode, this implementation incorrectly uses the event loop's
    /// file system thread instead of non-blocking. It needs to be reworked to properly
    /// use non-blocking I/O.
    pub fn write(self: Stream, buffer: []const u8) WriteError!usize {
        while (true) {
            var len = os.send(self.handle, buffer, 0) catch |err| switch (err) {
                error.WouldBlock => {
                    var wait = c.tb_socket_wait(
                        tbox.sockFromFd(self.handle),
                        c.TB_SOCKET_EVENT_SEND,
                        -1,
                    );
                    if (wait <= 0) return error.Unexpected;
                    continue;
                },
                else => |e| return e,
            };
            return len;
        }
    }

    pub fn writeAll(self: Stream, bytes: []const u8) WriteError!void {
        var index: usize = 0;
        while (index < bytes.len) {
            index += try self.write(bytes[index..]);
        }
    }

    /// See https://github.com/ziglang/zig/issues/7699
    /// See equivalent function: `std.fs.File.writev`.
    pub fn writev(self: Stream, iovecs: []const os.iovec_const) WriteError!usize {
        if (iovecs.len == 0) return 0;
        const first_buffer = iovecs[0].iov_base[0..iovecs[0].iov_len];
        try self.write(first_buffer);
        return first_buffer.len;
    }

    /// The `iovecs` parameter is mutable because this function needs to mutate the fields in
    /// order to handle partial writes from the underlying OS layer.
    /// See https://github.com/ziglang/zig/issues/7699
    /// See equivalent function: `std.fs.File.writevAll`.
    pub fn writevAll(self: Stream, iovecs: []os.iovec_const) WriteError!void {
        if (iovecs.len == 0) return;

        var i: usize = 0;
        while (true) {
            var amt = try self.writev(iovecs[i..]);
            while (amt >= iovecs[i].iov_len) {
                amt -= iovecs[i].iov_len;
                i += 1;
                if (i >= iovecs.len) return;
            }
            iovecs[i].iov_base += amt;
            iovecs[i].iov_len -= amt;
        }
    }
};

pub const StreamServer = struct {
    /// Copied from `Options` on `init`.
    kernel_backlog: u31,
    reuse_address: bool,
    reuse_port: bool,

    /// `undefined` until `listen` returns successfully.
    listen_address: Address,

    sockfd: ?os.socket_t,

    pub const Options = struct {
        /// How many connections the kernel will accept on the application's behalf.
        /// If more than this many connections pool in the kernel, clients will start
        /// seeing "Connection refused".
        kernel_backlog: u31 = 128,

        /// Enable SO.REUSEADDR on the socket.
        reuse_address: bool = false,

        /// Enable SO.REUSEPORT on the socket.
        reuse_port: bool = false,
    };

    /// After this call succeeds, resources have been acquired and must
    /// be released with `deinit`.
    pub fn init(options: Options) StreamServer {
        return StreamServer{
            .sockfd = null,
            .kernel_backlog = options.kernel_backlog,
            .reuse_address = options.reuse_address,
            .reuse_port = options.reuse_port,
            .listen_address = undefined,
        };
    }

    /// Release all resources. The `StreamServer` memory becomes `undefined`.
    pub fn deinit(self: *StreamServer) void {
        self.close();
        self.* = undefined;
    }

    pub fn listen(self: *StreamServer, address: Address) !void {
        const sock_flags = os.SOCK.STREAM | os.SOCK.CLOEXEC | os.SOCK.NONBLOCK;
        const proto = if (address.any.family == os.AF.UNIX) @as(u32, 0) else os.IPPROTO.TCP;

        const sockfd = try os.socket(address.any.family, sock_flags, proto);
        self.sockfd = sockfd;
        errdefer {
            tbox.closeSocket(sockfd);
            self.sockfd = null;
        }

        if (self.reuse_address) {
            try os.setsockopt(
                sockfd,
                os.SOL.SOCKET,
                os.SO.REUSEADDR,
                &mem.toBytes(@as(c_int, 1)),
            );
        }
        if (@hasDecl(os.SO, "REUSEPORT") and self.reuse_port) {
            try os.setsockopt(
                sockfd,
                os.SOL.SOCKET,
                os.SO.REUSEPORT,
                &mem.toBytes(@as(c_int, 1)),
            );
        }

        var socklen = address.getOsSockLen();
        try os.bind(sockfd, &address.any, socklen);
        try os.listen(sockfd, self.kernel_backlog);
        try os.getsockname(sockfd, &self.listen_address.any, &socklen);
    }

    /// Stop listening. It is still necessary to call `deinit` after stopping listening.
    /// Calling `deinit` will automatically call `close`. It is safe to call `close` when
    /// not listening.
    pub fn close(self: *StreamServer) void {
        if (self.sockfd) |fd| {
            tbox.closeSocket(fd);
            self.sockfd = null;
            self.listen_address = undefined;
        }
    }

    pub const AcceptError = error{
        ConnectionAborted,

        /// The per-process limit on the number of open file descriptors has been reached.
        ProcessFdQuotaExceeded,

        /// The system-wide limit on the total number of open files has been reached.
        SystemFdQuotaExceeded,

        /// Not enough free memory.  This often means that the memory allocation  is  limited
        /// by the socket buffer limits, not by the system memory.
        SystemResources,

        /// Socket is not listening for new connections.
        SocketNotListening,

        ProtocolFailure,

        /// Firewall rules forbid connection.
        BlockedByFirewall,

        FileDescriptorNotASocket,

        ConnectionResetByPeer,

        NetworkSubsystemFailed,

        OperationNotSupported,
    } || os.AcceptError || os.UnexpectedError;

    pub const Connection = struct {
        stream: Stream,
        address: Address,
    };

    /// If this function succeeds, the returned `Connection` is a caller-managed resource.
    pub fn accept(self: *StreamServer) AcceptError!Connection {
        var accepted_addr: Address = undefined;
        var adr_len: os.socklen_t = @sizeOf(Address);
        while (true) {
            const accept_result = os.accept(
                self.sockfd.?,
                &accepted_addr.any,
                &adr_len,
                os.SOCK.CLOEXEC | os.SOCK.NONBLOCK,
            );
            if (accept_result) |fd| {
                return Connection{
                    .stream = Stream{ .handle = fd },
                    .address = accepted_addr,
                };
            } else |err| switch (err) {
                error.WouldBlock => {
                    if (c.tb_socket_wait(
                        tbox.sockFromFd(self.sockfd.?),
                        c.TB_SOCKET_EVENT_ACPT,
                        -1,
                    ) <= 0) {
                        return error.Unexpected;
                    }
                },
                else => |e| return e,
            }
        }
    }
};
