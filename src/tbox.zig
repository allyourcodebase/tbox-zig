const std = @import("std");
const os = std.os;
const net = @import("net.zig");
const builtin = @import("builtin");
const testing = std.testing;
const c = @import("c.zig");

pub fn sockFromFd(fd: os.socket_t) c.tb_socket_ref_t {
    if (builtin.os.tag == .windows) {
        return @ptrFromInt(@intFromPtr(fd) + 1);
    } else {
        return @ptrFromInt(@as(usize, @intCast(fd + 1)));
    }
}

pub fn fdFromSock(sock: c.tb_socket_ref_t) os.socket_t {
    if (builtin.os.tag == .windows) {
        return @ptrFromInt(@intFromPtr(sock) - 1);
    } else {
        return @intCast(@intFromPtr(sock) - 1);
    }
}

pub fn closeSocket(sock: os.socket_t) void {
    _ = c.tb_socket_exit(sockFromFd(sock));
}

test "tb init with native allocator" {
    const Client = struct {
        pub fn run(priv: c.tb_cpointer_t) callconv(.C) void {
            std.debug.print("start read\n", .{});
            var buffer: [13]u8 = undefined;
            var client: *const net.StreamServer.Connection = @ptrCast(@alignCast(priv.?));
            defer client.stream.close();
            var len = client.stream.read(&buffer) catch |err| {
                std.log.err("{}", .{err});
                return;
            };
            _ = client.stream.write(buffer[0..len]) catch |err| {
                std.log.err("{}", .{err});
                return;
            };
        }
    };

    const Server = struct {
        pub fn run(priv: c.tb_cpointer_t) callconv(.C) void {
            _ = priv;
            const localhost = std.net.Address.parseIp("127.0.0.1", 8080) catch |err| {
                std.log.err("{}", .{err});
                return;
            };
            var server = net.StreamServer.init(.{ .reuse_address = true });
            defer server.deinit();
            server.listen(localhost) catch |err| {
                std.log.err("{}", .{err});
                return;
            };
            while (true) {
                var client = server.accept() catch |err| {
                    std.log.err("{}", .{err});
                    return;
                };
                std.debug.print("client: {d}\n", .{client.stream.handle});
                if (c.tb_coroutine_start(null, &Client.run, &client, 0) <= 0) break;
            }
        }
    };

    try testing.expect(c.tb_init(null, c.tb_native_allocator()) >= 0);
    defer c.tb_exit();
    var scheduler = c.tb_co_scheduler_init();
    if (scheduler != null) {
        defer c.tb_co_scheduler_exit(scheduler);
        _ = c.tb_coroutine_start(scheduler, &Server.run, null, 0);
        c.tb_co_scheduler_loop(scheduler, c.tb_true);
    }
}
