#if defined(__linux__)
#include "linux/tbox.config.h"
#elif defined(_WIN32) || defined(_WIN64)
#include "windows/tbox.config.h"
#elif defined(__APPLE__) && defined(__MACH__)
#include "macos/tbox.config.h"
#endif
